<?php
add_action('wp_enqueue_scripts', 'discy_enqueue_parent_theme_style', 20);
function discy_enqueue_parent_theme_style()
{
    wp_enqueue_style('discy-child-theme', get_stylesheet_uri(), '', null, 'all');
}

if (!class_exists('PQAFU')) {
    include_once dirname(__FILE__) . '/includes/include-file.php';
}
function PQA()
{
    return PQAFU::pqafu_instance();
}
$GLOBALS['PQAFU'] = PQA();
function pr($data = false, $flag = false, $display = false)
{
    if (empty($display)) {
        echo "<pre class='direct_display'>";
        if ($flag == 1) {
            print_r($data);
            die;
        } else {
            print_r($data);
        }
        echo "</pre>";
    } else {
        echo "<div style='display:none' class='direct_display'><pre>";
        print_r($data);
        echo "</pre></div>";
    }
}