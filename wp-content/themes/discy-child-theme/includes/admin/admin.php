<?php
add_action('add_meta_boxes', "create_meta_box");

function create_meta_box()
{
    $user = wp_get_current_user();
    if (count(array_intersect(array('author', 'administrator', 'editor'), (array)$user->roles)) > 0) {
        add_meta_box('membership_metabox', __("Make Premium", "rtc"), "display_meta_box", array('question'), 'side', 'high');
    }
}
function display_meta_box() {
    global $post;
    $selected_chk = get_post_meta($post->ID, 'premium_question', true);

    $type=(isset($_REQUEST['post']))?'edit':'add';
    $out = '<p class="wpqa_checkbox_p">
					<label for="premiumCheckbox">
						<span class="wpqa_checkbox"><input type="checkbox" id="premiumCheckbox" class="premium_input" name="premium_question" value="premium_question"' . ($type == "edit" && $selected_chk == "premium_question" ? " checked='checked'" : "") . '></span>
						<span class="wpqa_checkbox_span">' . esc_html__("Make question as a premium", "wpqa") . '</span>
					</label>
				</p>';
    echo $out;
}

add_action('save_post', 'save_metabox', 10, 1);

function save_metabox($id){
    $post_type = get_post_type($id);
    if ("question" != $post_type && "page" != $post_type) {
        return;
    }
    if (isset($_POST['premium_question']) && $_POST['premium_question'] != "") {
        update_post_meta($id, 'premium_question', esc_html($_POST['premium_question']));
    }
    else{
        delete_post_meta($id, 'premium_question');
    }
    if(isset($_POST['post_status']) && $_POST['post_status']=='publish'){
        if (wp_is_post_revision($_POST['ID'])) {
            return;
        }
        $post_author=$_POST['post_author'];
        $user_data=get_userdata($post_author);
        $name=$user_data->first_name.' '.$user_data->last_name;
        $user_email=$user_data->user_email;
        if(empty($name)){
            $name=$user_email;
        }
        $post_title=$_POST['post_title'];
        $post_link=get_the_permalink($_POST['ID']);
        $html=email_template($name,$post_link,$post_title);
        $headers[] = "MIME-Version: 1.0" . "\r\n";
        $headers[] = "Content-type: text/html; charset=UTF-8" . "\r\n";
        $subject = __("Question Approve on ".PQAFU_SITE_NAME, "discy");
        wp_mail($user_email, $subject, $html, $headers);
        wp_mail('sarfaraz@soften.io', $subject, $html, $headers);
    }
}

function email_template($name,$post_link,$post_title){
    return '<div style="margin:0;background-color:#f4f3f4;font-family:Helvetica,Arial,sans-serif;font-size:12px" text="#444" bgcolor="#F4F3F4" link="#21759B" alink="#21759B" vlink="#21759B" marginheight="0" marginwidth="0"><div class="adM">
			</div><table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#F4F3F4">
				<tbody>
				<tr>
				<td style="padding:15px">
					<center>
						<table width="550" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFF">
						<tbody>
						<tr>
						<td align="center">
						<div style="border:solid 1px #d9d9d9;padding-right:30px;padding-left:30px">
						<table style="line-height:1.6;font-size:12px;font-family:Helvetica,Arial,sans-serif;border:solid 1px #fff;color:#444" border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
						<tbody>
						<tr>
						<td style="color:#fff" colspan="2" valign="bottom" height="30"></td>
						</tr>
						<tr>
						<td style="line-height:32px;padding:30px 30px 20px;text-align:center;background-color:#fff" valign="baseline"><a href="'.site_url().'" target="_blank" ><img src="'.site_url().'/wp-content/uploads/2019/09/Pqafu_logo_final_ver01.png" class="CToWUd"></a></td>
						</tr>
						
						<tr>
						<td colspan="2">
						<div style="padding-top:10px;color:#444"><p>Hello, '.$name.'</p>
						
						<p>Congratulations! Your Question approved on '.PQAFU_PQAFU_NAME.'!</p>

						<p>Your Question: <a href="'.$post_link.'"> '.$post_title.'</a></p>
						<p><a href="'.site_url().'">Visit Site.</a></p>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
						</div>
						</td>
						</tr>
						 <!-- Spacing -->
                                <tr style="background: #000">
                                    <td height="10" style="font-size:1px;line-height:1px;mso-line-height-rule:exactly;">
                                         
                                    </td>
                                </tr>
                                <!-- Spacing -->
                                <tr style="background: #000">
                                    <td style="padding: 50px 0 0 0;">
                                        <!-- Social icons -->
                                        <table width="600" align="center" border="0" cellpadding="0" cellspacing="0"
                                               class="devicewidth">
                                            <tbody>
                                            <tr>
                                                <td align="center">
                                                    <h1 style="font-family:\'Montserrat\', serif;font-weight:lighter;color:#ffffff;">
                                                        Stay In Touch</h1>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center">
                                                    <a href="https://www.facebook.com/#"
                                                       style="font-size: 24px;margin: 20px 10px;color: #ffffff"
                                                       target="_blank"><img style="width: 40px"
                                                                            src="'.site_url().'/wp-content/uploads/2019/09/facebook-logo-button.png"
                                                                            alt="social icons"></a>
                                                    <a href="https://twitter.com/#"
                                                       style="font-size: 24px;margin: 20px 10px;color: #ffffff"
                                                       target="_blank"><img style="width: 40px"
                                                                            src="'.site_url().'/wp-content/uploads/2019/09/instagram-logo.png"
                                                                            alt="social icons"></a>
                                                    <a href="https://www.instagram.com/#/"
                                                       style="font-size: 24px;margin: 20px 10px;color: #ffffff"
                                                       target="_blank"><img style="width: 40px"
                                                                            src="'.site_url().'/wp-content/uploads/2019/09/twitter-logo-button.png"
                                                                            alt="social icons"></a></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p style="font-family:\'Montserrat\', serif;font-size: 10px;color: #ffffff;text-align: center;letter-spacing: 1px;margin-top: 30px;font-weight: 100">
                                                        © | 2019 All Rights Reserved.</p>
                                                </td>
                                            </tr>
                                            <tr>
                                            <td>
                                            <p>&nbsp;</p>
</td>
</tr>
                                            </tbody>
                                        </table>
                                        <!-- end of Social icons -->
                                    </td>
                                </tr>
                                <!-- Spacing -->
                                <tr style="background: #000">
                                
                                </tr>
                                <!-- Spacing -->
						</tbody>
						</table>
						</div>
						</td>
						</tr>
						</tbody>
						</table>
						
					</center>
				</td>
				</tr>
				</tbody>
			</table><div class="yj6qo"></div><div class="adL">
		</div></div>';
}

function email_template_BK($name,$post_link,$post_title){
    return '<table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" class="backgroundTable">
        <tbody>
        <tr>
            <td>
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                    <tbody>
                    <tr>
                        <td width="100%">
                            <table width="600" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0"
                                   align="center"
                                   class="devicewidth">
                                <tbody>
                                <tr>
                                    <td>
                                        <!-- logo -->
                                        <table bgcolor="#ffffff" width="100%" align="center" border="0" cellpadding="0"
                                               cellspacing="0" class="devicewidth">
                                            <tbody>
                                            <tr>
                                                <td align="center">
                                                    <div class="imgpop">
                                                        <a target="_blank" href="">
                                                            <img src="'.site_url().'/wp-content/uploads/2019/09/Pqafu_logo_final_ver01.png"
                                                                 alt="Logo" border="0" style="display:block; border:none; outline:none; text-decoration:none;margin: 30px 0"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!-- end of logo -->
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" class="backgroundTable">
        <tbody>
        <tr>
            <td>
                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
                    <tbody>
                    <tr>
                        <td width="100%">
                            <table width="600" align="center" cellspacing="0" cellpadding="0" border="0"
                                   class="devicewidth">
                                <tbody>
                                <tr>
                                    <!-- start of image -->
                                    <td align="center">
                                        <h1 style="font-family:\'Montserrat\', serif;font-weight:bolder;margin:20px 0 50px 0;font-size: 36px;letter-spacing: 1px">Question Approved</h1>
                                        <p style="font-family:\'Montserrat\', serif;text-align: left;font-size: 18px;font-weight: 600;letter-spacing: 1px">
                                            Hello, '.$name
        .'</p>
                                        <p style="font-family:\'Montserrat\', serif;text-align: left;font-size: 14px;font-weight: lighter;margin-top: 20px">Congratulations! Your Question approved on PQAFU!</p>
                                        
                                        <p style="font-family:\'Montserrat\', serif;text-align: left;font-size: 14px;font-weight: lighter;margin-top: 20px">Your Question: <a href="'.$post_link.'"> '.$post_title.'</a></p>
                                        
                                    </td>
                                </tr></tbody>
                            </table>
                            <!-- end of image -->
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" class="backgroundTable">
        <tbody>
        <tr>
            <td>
                <table width="600" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth">
                    <tbody>
                    <tr>
                        <td align="center" height="25" style="font-size:1px;line-height:1px;">
                             
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <table width="100%" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" class="backgroundTable">
        <tbody>
        <tr>
            <td>
                <table width="600" bgcolor="#ffffff" cellpadding="0" cellspacing="0" border="0" align="center"
                       class="devicewidth">
                    <tbody>
                    <tr>
                        <td width="100%" style="padding: 10px 0;">
                            <table bgcolor="#000000" width="600" cellpadding="0" cellspacing="0" border="0"
                                   align="center"
                                   class="devicewidth">
                                <tbody>
                                <!-- Spacing -->
                                <tr>
                                    <td height="10" style="font-size:1px;line-height:1px;mso-line-height-rule:exactly;">
                                         
                                    </td>
                                </tr>
                                <!-- Spacing -->
                                <tr>
                                    <td style="padding: 50px 0 0 0;">
                                        <!-- Social icons -->
                                        <table width="600" align="center" border="0" cellpadding="0" cellspacing="0"
                                               class="devicewidth">
                                            <tbody>
                                            <tr>
                                                <td align="center">
                                                    <h1 style="font-family:\'Montserrat\', serif;font-weight:lighter;color:#ffffff;">
                                                        Stay In Touch</h1>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: center">
                                                    <a href="https://www.facebook.com/#"
                                                       style="font-size: 24px;margin: 20px 10px;color: #ffffff"
                                                       target="_blank"><img style="width: 40px"
                                                                            src="'.site_url().'/wp-content/uploads/2019/09/facebook-logo-button.png"
                                                                            alt="social icons"></a>
                                                    <a href="https://twitter.com/#"
                                                       style="font-size: 24px;margin: 20px 10px;color: #ffffff"
                                                       target="_blank"><img style="width: 40px"
                                                                            src="'.site_url().'/wp-content/uploads/2019/09/instagram-logo.png"
                                                                            alt="social icons"></a>
                                                    <a href="https://www.instagram.com/#/"
                                                       style="font-size: 24px;margin: 20px 10px;color: #ffffff"
                                                       target="_blank"><img style="width: 40px"
                                                                            src="'.site_url().'/wp-content/uploads/2019/09/twitter-logo-button.png"
                                                                            alt="social icons"></a></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p style="font-family:\'Montserrat\', serif;font-size: 10px;color: #ffffff;text-align: center;letter-spacing: 1px;margin-top: 30px;font-weight: 100">
                                                        © | 2019 All Rights Reserved.</p>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!-- end of Social icons -->
                                    </td>
                                </tr>
                                <!-- Spacing -->
                                <tr>
                                    <td height="10" style="font-size:1px;line-height:1px;mso-line-height-rule:exactly;">
                                         
                                    </td>
                                </tr>
                                <!-- Spacing -->
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>';
}

function wpb_sender_email($original_email_address)
{
    return PQAFU_ADMIN_EMAIL;
}

function wpb_sender_name($original_email_from)
{
    return PQAFU_PQAFU_NAME;
}

add_filter('wp_mail_from', 'wpb_sender_email');
add_filter('wp_mail_from_name', 'wpb_sender_name');