<?php

/**
 * PQAFU setup
 *
 * @package PQAFU
 * @since    1.0.0
 */
defined('ABSPATH') || exit;

/**
 * Main PQAFU Lite Class.
 *
 * @class PQAFU
 * @author Sarfaraj Kazi
 */
final class PQAFU
{

    /**
     * The single instance of the class.
     *
     * @since 2.1
     */
    protected static $_instance = null;
    /**
     * PQAFU Lite version.
     *
     * @var string
     */
    public $version = '1.0.0';
    /**
     * Session instance.
     *
     * @var PQAFU_Session|PQAFU_Session_Handler
     */
    public $session = null;

    /**
     * Query instance.
     *
     * @var PQAFU_Query
     */
    public $query = null;

    /**
     * pqafu Constructor.
     */
    public function __construct()
    {
        $this->pqafu_define_constants();
        add_action('init', array($this, 'init_callback'));
        $this->pqafu_includes();
    }

    /**
     * Define Constants
     */
    function pqafu_define_constants()
    {
        global $wpdb;
        $upload_dir = wp_upload_dir();
        $random_number = random_int(111111, 999999999);

        /*
         * plugin constants
         */
        $this->pqafu_define("PQAFU_URL", trailingslashit(get_stylesheet_directory_uri()));

        $this->pqafu_define("PQAFU_PATH", trailingslashit(get_stylesheet_directory()));

        $this->pqafu_define("PQAFU_ASSETS_URL", PQAFU_URL . 'assets/');
        $this->pqafu_define("PQAFU_ADMIN_CSS", PQAFU_ASSETS_URL . 'css/admin/');
        $this->pqafu_define("PQAFU_ADMIN_JS", PQAFU_ASSETS_URL . 'js/admin/');
        $this->pqafu_define("PQAFU_FRONT_CSS", PQAFU_ASSETS_URL . 'css/frontend/');
        $this->pqafu_define("PQAFU_FRONT_JS", PQAFU_ASSETS_URL . 'js/frontend/');


        $this->pqafu_define("PQAFU_ASSETS_PATH", PQAFU_PATH . 'assets/');
        $this->pqafu_define("PQAFU_ADMIN_CSS_PATH", PQAFU_ASSETS_PATH . 'css/admin/');
        $this->pqafu_define("PQAFU_ADMIN_JS_PATH", PQAFU_ASSETS_PATH . 'js/admin/');
        $this->pqafu_define("PQAFU_FRONT_CSS_PATH", PQAFU_ASSETS_PATH . 'css/frontend/');
        $this->pqafu_define("PQAFU_FRONT_JS_PATH", PQAFU_ASSETS_PATH . 'js/frontend/');


        $this->pqafu_define('PQAFU_VERSION', $this->version);
        $this->pqafu_define('PQAFU_DATE_FORMAT', get_option('date_format', true));
        $this->pqafu_define('PQAFU_TIME_FORMAT', get_option('time_format', true));
        $this->pqafu_define('PQAFU_DB_DATE_FORMAT', "Y-m-d H:i:s");
        /*
         * urls and site info
         */
        $this->pqafu_define("PQAFU_ADMIN_URL", admin_url());
        $this->pqafu_define("PQAFU_ADMIN_AJAX_URL", admin_url('admin-ajax.php'));
        $this->pqafu_define("PQAFU_SITE_URL", trailingslashit(site_url()));
        $this->pqafu_define("PQAFU_SITE_NAME", get_bloginfo('name'));
        $this->pqafu_define("PQAFU_PQAFU_NAME", get_option('blogname'));
        $this->pqafu_define("PQAFU_SITE_DESC", get_bloginfo('description'));
        $this->pqafu_define("PQAFU_ADMIN_EMAIL", get_bloginfo('admin_email'));
        $this->pqafu_define('PQAFU_ENABLE', 1);
        $this->pqafu_define('PQAFU_DISABLE', 0);
        $this->pqafu_define('PQAFU_RANDOM_NUMBER', $random_number);
        $this->pqafu_define('PQAFU_POST_TYPE', 'pqafu');
        $this->pqafu_define('LANGUAGE_DOMAIN', 'pqafu_domain');
    }

    /**
     * Define constant if not already set.
     *
     * @param string $name Constant name.
     * @param string|bool $value Constant value.
     */
    private function pqafu_define($name, $value)
    {
        if (!defined($name)) {
            define($name, $value);
        }
    }

    /**
     * Include required core files used in admin and on the frontend.
     */
    function pqafu_includes()
    {
        if ($this->pqafu_is_request('admin')) {
            include_once PQAFU_PATH . 'includes/admin/admin.php';
        }
        if ($this->pqafu_is_request('frontend')) {
            include_once PQAFU_PATH . 'includes/frontend/frontend.php';
        }
    }

    /**
     * What type of request is this?
     *
     * @param string $type admin or frontend.
     * @return bool
     */
    private function pqafu_is_request($type)
    {
        switch ($type) {
            case 'admin':
                return (is_admin() || defined('DOING_AJAX'));
            case 'frontend':
                return (!is_admin() || defined('DOING_AJAX'));
        }
    }

    /**
     * Main PQAFU Instance.
     *
     * Ensures only one instance of PQAFU is loaded or can be loaded.
     *
     * @return PQAFU - Main instance.
     * @see pqafu_getInstance()
     * @since 1.0.0
     * @static
     */
    public static function pqafu_instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    function init_callback()
    {
//        pr(PQAFU_URL,1);
    /*    pr(wpqa_options('ask_question_items'));
        pr(wpqa_options('ask_user_items'),1);*/
    }
}
