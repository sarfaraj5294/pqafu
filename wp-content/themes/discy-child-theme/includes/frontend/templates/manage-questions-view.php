<?php
if (!is_user_logged_in()) {
    return;
}
?>
<div class="manage-table-div">
    <table class="manage-table">
        <tr>
            <th>
                <?php
                _e("No.", "discy");
                ?>
            </th>
            <th>
                <?php
                _e("Question", "discy");
                ?>
            </th>
            <th>
                <?php
                _e("Status", "discy");
                ?>
            </th>
            <th>
                <?php
                _e("Create on", "discy");
                ?>
            </th>
            <th>
                <?php
                _e("Actions", "discy");
                ?>
            </th>
        </tr>
        <tr>
            <?php
            $array_args = array(
                'post_type' => 'question',
                'author' => get_current_user_id(),
                'orderby' => 'post_date',
                'posts_per_page' => -1,
                'post_status'=>array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash')
            );
            $posts = get_posts($array_args);
            if ($posts) {
                $i=1;
                foreach ($posts as $single_post) {
                    ?>
                    <td>
                        <?php echo $i++; ?>
                    </td>
                    <td>
                        <?php echo ucfirst($single_post->post_title); ?>
                    </td>
                    <td>
                        <?php
                        $status_of_post=$single_post->post_status;
                        if(strtolower($status_of_post)=='publish'){
                            $cls='publish-class';
                        }
                        else{
                            $cls='draft-class';
                        }
                        ?>
                        <label class="<?php echo $cls ?>">
                            <?php echo ucfirst($single_post->post_status); ?>
                        </label>
                    </td>
                    <td>
                        <?php echo date(PQAFU_DATE_FORMAT.' '.PQAFU_TIME_FORMAT,strtotime($single_post->post_date)); ?>
                    </td>
                    <td>
                        <a href="<?php echo get_the_permalink($single_post->ID) ?>" target="_blank">
                            <?php _e("View Question") ?>
                        </a>
                    </td>
                    <?php
                }
            }
            ?>
        </tr>
    </table>
</div>