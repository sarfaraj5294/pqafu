<?php


if (!defined('ABSPATH')) {
    exit;
}

/**
 * Handle all Filters Requests
 * @class pqafu_actions_handler
 * @since 1.0.0
 * @author Sarfaraj Kazi
 */
class pqafu_filters_handler
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        add_filter('wpqa_add_question_fields',[$this,'wpqa_add_question_fields_callback']);
        add_filter('wpqa_edit_question_fields',[$this,'wpqa_add_question_fields_callback']);
    }
    function wpqa_add_question_fields_callback($fields){
        if(is_user_logged_in() && pmpro_hasMembershipLevel()){
            $fields[]='premium_question';
            return $fields;
        }
        return $fields;
    }
}

return new pqafu_filters_handler();