<?php

if (!defined('ABSPATH')) {
    exit;
}
/**
 * Handle Front Scripts and StyleSheets
 * @class pqafu_front_assets
 * @since 1.0.0
 */
if (!class_exists('pqafu_frontend_assets', false)) :

    class pqafu_frontend_assets
    {
        /**
         * Hook in tabs.
         */
        public function __construct()
        {
            add_action('wp_enqueue_scripts', array($this, 'pqafu_frontend_styles'),999);
            add_action('wp_enqueue_scripts', array($this, 'pqafu_frontend_scripts'));
        }

        /**
         * Enqueue styles.
         */
        public function pqafu_frontend_styles()
        {

            $cssVersion = filemtime(PQAFU_FRONT_CSS_PATH . 'front_custom_css.css');
            wp_enqueue_style('pqafu_custom-style', PQAFU_FRONT_CSS . 'front_custom_css.css', array(), $cssVersion);
        }

        /**
         * Enqueue scripts.
         */
        public function pqafu_frontend_scripts()
        {
            $jsVersion = filemtime(PQAFU_FRONT_JS_PATH . 'front_custom_js.js');
            wp_enqueue_script('pqafu_custom-js-file', PQAFU_FRONT_JS . 'front_custom_js.js', array('jquery'), $jsVersion, true);
            wp_localize_script('pqafu_custom-js-file', 'frontend_veriables', array(
                'ajax_url' => PQAFU_ADMIN_URL,
                'site_url' => PQAFU_SITE_URL,
                'is_logged_in' => is_user_logged_in() ? is_user_logged_in() : ''
            ));
        }

    }
endif;
return new pqafu_frontend_assets();
