<?php


if (!defined('ABSPATH')) {
    exit;
}

/**
 * Handle all Actions Requests
 * @class pqafu_actions_handler
 * @since 1.0.0
 * @author Sarfaraj Kazi
 */
class pqafu_actions_handler
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        add_action('wp_head', [$this, 'wp_head_hide_billing_fields']);
        add_action('pmpro_required_billing_fields', [$this, 'my_pmpro_required_billing_fields']);
        add_action('init', [$this, 'handle_init_request'],1);
    }

    function wp_head_hide_billing_fields()
    {
        global $post, $pmpro_pages;
        if (empty($pmpro_pages) || (!is_page($pmpro_pages['checkout']) && !is_page($pmpro_pages['billing'])))
            return;
        ?>
        <style>
            #pmpro_billing_address_fields, #pmpro_account_loggedin {
                display: none;
            }

        </style>
        <?php
    }

    function my_pmpro_required_billing_fields($fields)
    {
        if (is_array($fields)) {
            unset($fields['bfirstname']);
            unset($fields['blastname']);
            unset($fields['baddress1']);
            unset($fields['baddress2']);
            unset($fields['bcity']);
            unset($fields['bstate']);
            unset($fields['bzipcode']);
            unset($fields['bcountry']);
            unset($fields['bphone']);
        }
        return $fields;
    }

    function handle_init_request()
    {

    }
}

return new pqafu_actions_handler();