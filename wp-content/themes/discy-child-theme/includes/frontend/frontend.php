<?php

if (!defined('ABSPATH')) {
    exit;
}

/**
 * Handle all Front Requests
 *
 * @class pqafu_frontend
 * @since 1.0.0
 * @author Sarfaraj Kazi
 */
if (!class_exists('pqafu_frontend', false)) :
    class pqafu_frontend
    {
        /**
         * Constructor.
         */
        public function __construct()
        {
            include_once PQAFU_PATH . 'includes/frontend/assets.php';
            include_once PQAFU_PATH . 'includes/frontend/actions.php';
            include_once PQAFU_PATH . 'includes/frontend/filters.php';
            include_once PQAFU_PATH . 'includes/frontend/shortcodes.php';
        }

    }
endif;
return new pqafu_frontend();
