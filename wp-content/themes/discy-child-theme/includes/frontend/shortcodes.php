<?php


if (!defined('ABSPATH')) {
    exit;
}

/**
 * Handle all Shortcodes Requests
 * @class pqafu_shortcodes_handler
 * @since 1.0.0
 * @author Sarfaraj Kazi
 */
class pqafu_shortcodes_handler
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        add_shortcode('manage_questions',[$this,'mamange_questionns_callback']);
    }
    function mamange_questionns_callback(){
        ob_start();
        include_once 'templates/manage-questions-view.php';
        $html=ob_get_contents();
        ob_end_clean();
        return $html;
    }

}

return new pqafu_shortcodes_handler();